<?php
/**
 * Created by PhpStorm.
 * User: jonathan
 * Date: 16/05/2017
 * Time: 14:02
 */

namespace AppBundle\Entity;


use Symfony\Component\BrowserKit\Request;

class Person
{
    private $repo;
    private $id;
    private $name;
    private $first_name;
    private $location;
    private $phone;
    private $cell;
    private $email;
    private $type;
    private $error;

    public function __construct($id = null, $name = null, $first, $loc, $email, $phone, $cell, $type)
    {
        if ($id == null) {
            $pdo = $this->connect();
            $this->id = $id;
            $this->name = $name;
            $this->first_name = $first;
            $this->location = $loc;
            $this->email = $email;
            $this->phone = $phone;
            $this->cell = $cell;
            $this->type = $type;
            try {
                $pdo->beginTransaction();
                $statement = $pdo->prepare('INSERT INTO persons ' . '(name,first_name,location,phone,cell,email,type)' .
                    ' VALUES (?, ?, ?, ?, ?, ?, ?);');
                $statement->bindParam(1, $this->name, \PDO::PARAM_STR);
                $statement->bindParam(2, $this->first_name, \PDO::PARAM_STR);
                $statement->bindParam(3, $this->location, \PDO::PARAM_STR);
                $statement->bindParam(4, $this->phone, \PDO::PARAM_STR);
                $statement->bindParam(5, $this->cell, \PDO::PARAM_STR);
                $statement->bindParam(6, $this->email, \PDO::PARAM_STR);
                $statement->bindParam(7, $this->type, \PDO::PARAM_STR);
                $statement->execute();
                $pdo->commit();
                $this->error = "";
            } catch (\PDOException $e) {
                $this->error = $e->getMessage();
            }
        } else {
            if ($name == null) {
                $pdo = $this->connect();
                try {
                    $pdo->beginTransaction();
                    $this->id = $id;
                    $statement = $pdo->prepare("DELETE FROM persons WHERE id=?;");
                    $statement->bindParam(1, $this->id, \PDO::PARAM_INT);
                    $statement->execute();
                    $pdo->commit();
                    $this->error = "";
                } catch (\PDOException $e) {
                    $this->error = $e->getMessage();
                    $pdo->rollBack();
                }
            } else {
                $pdo = $this->connect();
                $this->id = $id;
                $this->name = $name;
                $this->first_name = $first;
                $this->location = $loc;
                $this->email = $email;
                $this->phone = $phone;
                $this->cell = $cell;
                $this->type = $type;
                try {
                    $pdo->beginTransaction();
                    $statement = $pdo->prepare("UPDATE persons SET name = ?, first_name = ?, location = ?, phone = ?, " .
                    "cell = ?, email = ?, type = ? WHERE id = ?");
                    $statement->bindParam(1, $this->name, \PDO::PARAM_STR);
                    $statement->bindParam(2, $this->first_name, \PDO::PARAM_STR);
                    $statement->bindParam(3, $this->location, \PDO::PARAM_STR);
                    $statement->bindParam(4, $this->phone, \PDO::PARAM_STR);
                    $statement->bindParam(5, $this->cell, \PDO::PARAM_STR);
                    $statement->bindParam(6, $this->email, \PDO::PARAM_STR);
                    $statement->bindParam(7, $this->type, \PDO::PARAM_STR);
                    $statement->bindParam(8, $this->id, \PDO::PARAM_INT);
                    $statement->execute();
                    $pdo->commit();
                    $this->error = "";
                } catch (\PDOException $e) {
                    $this->error = $e->getMessage();
                    $pdo->rollBack();
                }
            }
        }
        $pdo = null;
    }
        private function connect()
    {
        $pdo = new \PDO("mysql:host=localhost;dbname=webproject", "root", "webpxl753");
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        return $pdo;
    }

        /**
         * @return mixed
         */
        public function getId()
    {
        return $this->id;
    }

        /**
         * @return mixed
         */
        public function getName()
    {
        return $this->name;
    }

        /**
         * @return mixed
         */
        public function getFirstName()
    {
        return $this->first_name;
    }

        /**
         * @return mixed
         */
        public function getLocation()
    {
        return $this->location;
    }

        /**
         * @return mixed
         */
        public function getPhone()
    {
        return $this->phone;
    }

        /**
         * @return mixed
         */
        public function getCell()
    {
        return $this->cell;
    }

        /**
         * @return mixed
         */
        public function getEmail()
    {
        return $this->email;
    }

        /**
         * @return mixed
         */
        public function getType()
    {
        return $this->type;
    }

        /**
         * @return string
         */
        public function getError(): string
    {
        return $this->error;
    }


    }