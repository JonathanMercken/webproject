<?php
/**
 * Created by PhpStorm.
 * User: jonathan
 * Date: 11/05/2017
 * Time: 17:27
 */

namespace AppBundle\Entity;


use AppBundle\Controller\LoginController;
use Doctrine\DBAL\Driver\PDOException;

class Login
{
    private $username;
    private $password;
    private $loggedIn;
    private $error;

    private function connect() {
        $pdo = new \PDO("mysql:host=localhost;dbname=webproject", "root", "webpxl753");
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        return $pdo;
    }

    public function __construct($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
        $this->error = null;
        $pdo = $this->connect();
        try {
            $statement = $pdo->prepare("SELECT * FROM login WHERE username = ? AND password = ?;");
            $statement->setFetchMode(\PDO::FETCH_ASSOC);
            $statement->bindParam(1, $this->username, \PDO::PARAM_STR);
            $statement->bindParam(2, $this->password, \PDO::PARAM_STR);
            $statement->execute();
            if($statement->rowCount() > 0) {
                while ($row = $statement->fetch()) {
                    $this->loggedIn = true;
                    $this->error = null;
                    $this->username = $row['username'];
                    $this->password = $row['password'];
                }
            }
            else {
                $this->loggedIn = false;
                $this->error = "Error: geef geldige invoer";
            }
        } catch (PDOException $e) {
            $this->loggedIn = false;
            $this->error = $e->getMessage();
        }
        $pdo = null;
    }

    /**
     * @return string
     */
    public function getError() {
        return $this->error;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getLoggedIn()
    {
        return $this->loggedIn;
    }

}