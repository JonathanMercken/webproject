<?php
/**
 * Created by PhpStorm.
 * User: jonathan
 * Date: 16/05/2017
 * Time: 13:33
 */

namespace AppBundle\Entity;


class PersonRepository
{
    private $results = array();
    private $error;

    private function connect()
    {
        $pdo = new \PDO("mysql:host=localhost;dbname=webproject", "root", "webpxl753");
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        return $pdo;
    }

    public function __construct()
    {
        $pdo = $this->connect();
        try {
            $statement = $pdo->prepare("SELECT * FROM persons;");
            $statement->setFetchMode(\PDO::FETCH_ASSOC);
            $statement->execute();
            while ($row = $statement->fetch()) {
                $this->results[] = $row;
            }
        } catch (\PDOException $e) {
            $this->error = $e->getMessage();
        }
        $pdo = null;
    }
    /**
     * @return string
     */
    public function getError() {
        return $this->error;
    }

    /**
     * @return array
     */
    public function getResults(): array
    {
        return $this->results;
    }

}