<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Login;
use AppBundle\Entity\Person;
use AppBundle\Entity\PersonRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class LoginController extends Controller
{
    /**
     * @Route("/r", name="reroute")
     */
    public function reroute() {
        $GLOBALS = array();
        return $this->redirect("/");
    }

    /**
     * @Route("/", name="homepage")
     */
    public function have_to_login(Request $request){
        $post = Request::createFromGlobals();
        if($post->request->has('submit')) {
            $name = $post->request->get('name');
            $first = $post->request->get('first_name');
            $loc = $post->request->get('location');
            $phone = $post->request->get('phone');
            $cell = $post->request->get('cell');
            $email = $post->request->get('email');
            $type = $post->request->get('type');
            $person = new Person(null, $name, $first, $loc, $email, $phone, $cell, $type);
            $repo = new PersonRepository();
            return $this->render('WP4/table.html.twig', array('error'=>$person->getError(), 'repo'=>$repo->getResults()));
        }
        else if($post->request->has('update') || $post->request->has('delete')) {
            if($post->request->has('update')) {
                $id = $post->request->get('id');
                $name = $post->request->get('name');
                $first = $post->request->get('first_name');
                $loc = $post->request->get('location');
                $phone = $post->request->get('phone');
                $cell = $post->request->get('cell');
                $email = $post->request->get('email');
                $type = $post->request->get('type');
                $person = new Person($id, $name, $first, $loc, $email, $phone, $cell, $type);
                $repo = new PersonRepository();
                return $this->render('WP4/table.html.twig', array('error'=>$person->getError(), 'repo'=>$repo->getResults()));
            } else {
                $id = $post->request->get('id');
                $person = new Person($id, null, null, null, null, null, null, null);
                $repo = new PersonRepository();
                return $this->render('WP4/table.html.twig', array('error'=>$person->getError(), 'repo'=>$repo->getResults()));
            }
        }
        else if($post->request->has('login')) {
            $username = $post->request->get('login_name');
            $password = $post->request->get('login_password');
            $session = new Login($username, $password);
            if($session->getLoggedIn()) {
                $repo = new PersonRepository();
                return $this->render('WP4/table.html.twig', array('error'=>null, 'repo'=>$repo->getResults()));
            }
            else {
                return $this->render('WP4/login.html.twig', array('error'=>$session->getError()));
            }
        }
        return $this->render('WP4/login.html.twig', array('error'=>null));
    }
}