<?php
namespace AppBundle\Controller;

use \AppBundle\models\Ticket;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Created by PhpStorm.
 * User: jonathan
 * Date: 14/04/2017
 * Time: 16:57
 */
class TicketController extends Controller
{
    /**
     * @Route("/ticketing/{id}", requirements={"id": "\d+"})
     */
    public function getTicket($id) {
        $ticket = new Ticket($id, null, null);
        return $this->render('ticket/number.html.twig', array('number'=>$ticket->getId(), 'firstName'=>$ticket->getFirstName(),
            'lastName'=>$ticket->getLastName()));
    }
    /**
     * @Route("/ticketing/")
     */
    public function createTicket() {
        $post = Request::createFromGlobals();
        if($post->request->has('submit')){
            $firstName = $post->request->get('firstName');
            $lastName = $post->request->get('lastName');
            $ticket = new Ticket(null, $firstName, $lastName);
        }
        else {
            $firstName = null;
            $lastName = null;
        }
        return $this->render('ticket/create.html.twig', array('firstName' => $firstName, 'lastName' => $lastName));
    }
}