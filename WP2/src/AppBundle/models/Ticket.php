<?php
/**
 * Created by PhpStorm.
 * User: jonathan
 * Date: 14/04/2017
 * Time: 17:36
 */

namespace AppBundle\models;


use Doctrine\DBAL\Driver\PDOConnection;
use Doctrine\DBAL\Driver\PDOException;

class Ticket
{
    // TODO: error-view maken, entities zelf laten genereren
    private $id;
    private $firstName;
    private $lastName;

    private function connect() {
        $pdo = new \PDO("mysql:host=localhost;dbname=webproject", "root", "webpxl753");
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        return $pdo;
    }

    public function __construct($id = null, $firstName = null, $lastName = null)
    {
        $pdo = $this->connect();
        if($id == null) {
            $this->id = $id;
            $this->firstName = $firstName;
            $this->lastName = $lastName;
            try {
                $pdo->beginTransaction();
                $statement = $pdo->prepare('INSERT INTO Tickets ' . '(Naam, Voornaam) VALUES (?, ?);');
                $statement->bindParam(1, $this->lastName, \PDO::PARAM_STR);
                $statement->bindParam(2, $this->firstName, \PDO::PARAM_STR);
                $statement->execute();
                $pdo->commit();
            }
            catch(PDOException $exception) {
                echo "Error: " . $exception->getMessage();
                $pdo->rollBack();
            }
        }
        else {
            $this->id = $id;
            try {
                $statement = $pdo->prepare("SELECT * FROM Tickets WHERE Id = ?;");
                $statement->setFetchMode(\PDO::FETCH_ASSOC);
                $statement->bindParam(1, $this->id, \PDO::PARAM_INT);
                $statement->execute();
                while($row = $statement->fetch()) {
                    $this->firstName = $row['Voornaam'];
                    $this->lastName = $row['Naam'];
                }
            } catch(PDOException $exception) {
                echo "Error: " . $exception->getMessage();
                $pdo->rollBack();
            }
        }
        $pdo = null;
    }

    /**
     * @return array
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

}