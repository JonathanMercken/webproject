<?php
/**
 * Created by PhpStorm.
 * User: jonathan
 * Date: 29/03/2017
 * Time: 14:28
 */

namespace model;


class Person
{
    private $id;
    private $naam;
    private $voornaam;
    private $telnummer;
    private $gsmnummer;
    private $email;
    private $type;
    private $locatie;


    public function __construct($id, $naam, $voornaam, $loonkost, $telnummer, $gsmnummer, $email, $type, $locatie)
    {
        $this->id = $id;
        $this->naam = $naam;
        $this->voornaam = $voornaam;
        $this->loonkost = $loonkost;
        $this->telnummer = $telnummer;
        $this->gsmnummer = $gsmnummer;
        $this->email = $email;
        $this->type = $type;
        $this->locatie = $type;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNaam()
    {
        return $this->naam;
    }

    /**
     * @param mixed $naam
     */
    public function setNaam($naam)
    {
        $this->naam = $naam;
    }

    /**
     * @return mixed
     */
    public function getVoornaam()
    {
        return $this->voornaam;
    }

    /**
     * @param mixed $voornaam
     */
    public function setVoornaam($voornaam)
    {
        $this->voornaam = $voornaam;
    }

    /**
     * @return mixed
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param mixed $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @return mixed
     */
    public function getGemeente()
    {
        return $this->gemeente;
    }

    /**
     * @param mixed $gemeente
     */
    public function setGemeente($gemeente)
    {
        $this->gemeente = $gemeente;
    }

    /**
     * @return mixed
     */
    public function getStraat()
    {
        return $this->straat;
    }

    /**
     * @param mixed $straat
     */
    public function setStraat($straat)
    {
        $this->straat = $straat;
    }

    /**
     * @return mixed
     */
    public function getHuisnummer()
    {
        return $this->huisnummer;
    }

    /**
     * @param mixed $huisnummer
     */
    public function setHuisnummer($huisnummer)
    {
        $this->huisnummer = $huisnummer;
    }

    /**
     * @return mixed
     */
    public function getTelnummer()
    {
        return $this->telnummer;
    }

    /**
     * @param mixed $telnummer
     */
    public function setTelnummer($telnummer)
    {
        $this->telnummer = $telnummer;
    }

    /**
     * @return mixed
     */
    public function getGsmnummer()
    {
        return $this->gsmnummer;
    }

    /**
     * @param mixed $gsmnummer
     */
    public function setGsmnummer($gsmnummer)
    {
        $this->gsmnummer = $gsmnummer;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getLocatie()
    {
        return $this->locatie;
    }

    /**
     * @param mixed $locatie
     */
    public function setLocatie($locatie)
    {
        $this->locatie = $locatie;
    }
}