<?php
/**
 * Created by PhpStorm.
 * User: jonathan
 * Date: 29/03/2017
 * Time: 14:28
 */

namespace model;

class PDOEventRepository implements EventRepository
{
    private $connection = null;

    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }

    public function deleteEvent($id)
    {
        try {
            $statement = $this->connection->prepare('DELETE FROM events WHERE Id=?');
            $statement->bindParam(1, $id, \PDO::PARAM_INT);
            $numberRows = $statement->execute();
            print("$numberRows rows modified");
            return $numberRows;
        } catch (PDOException $e) {
            print 'Exception!: ' . $e->getMessage();
        }
        $pdo = null;
    }

    public function findEventById($id)
    {
        try {
            $statement = $this->connection->prepare('SELECT * FROM events WHERE Id = ?');
            $statement->bindParam(1, $id, \PDO::PARAM_INT);
            $statement->execute();
            $results = $statement->fetchAll(\PDO::FETCH_ASSOC);
            if (count($results) > 0) {
                return new Event($results[0]['Id'], $results[0]['name'], $results[0]['start'], $results[0]['end'], $results[0]['location'], $results[0]['cost'], $results[0]['personId']);
            } else {
                throw new \Exception();
            }
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function findEvents()
    {
        try {
            $statement = $this->connection->prepare('SELECT * FROM events');
            $statement->execute();
            $eventArray = array();
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            for ($i = 0; $i < count($result); $i++) {
                $eventArray[$i] = new Event($result[$i]['Id'], $result[$i]['name'], $result[$i]['start'], $result[$i]['end'], $result[$i]['location'], $result[$i]['cost'], $result[$i]['personId']);
            }
            return $eventArray;
        } catch (\Exception $ex) {
            return null;
        }
    }

    public function findEventByPerson($personId)
    {
        try {
            $statement = $this->connection->prepare('SELECT * FROM events WHERE personId=?');
            $statement->bindParam(1, $personId, \PDO::PARAM_INT);
            $statement->execute();
            $eventArray = array();
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);

            for ($i = 0; $i < count($result); $i++) {
                $eventArray[$i] = new Event($result[$i]['Id'], $result[$i]['name'], $result[$i]['start'], $result[$i]['end'], $result[$i]['location'], $result[$i]['cost'], $result[$i]['personId']);
            }

            return $eventArray;
        } catch (\Exception $ex) {
            return null;
        }
    }

    public function findEventByDate($begindate, $endDate)
    {
        try {
            $statement = $this->connection->prepare('SELECT * FROM events WHERE start BETWEEN ? AND ?');
            $statement->bindParam(1, $begindate, \PDO::PARAM_STR);
            $statement->bindParam(2, $endDate, \PDO::PARAM_STR);
            $statement->execute();
            $eventArray = array();
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);

            for ($i = 0; $i < count($result); $i++) {
                $eventArray[$i] = new Event($result[$i]['Id'], $result[$i]['name'], $result[$i]['start'], $result[$i]['end'], $result[$i]['location'], $result[$i]['cost'], $result[$i]['personId']);
            }

            return $eventArray;
        } catch (\Exception $ex) {
            return null;
        }
    }

    public function findEventByPersonAndDate($personId, $begindate, $endDate)
    {
        try {
            $statement = $this->connection->prepare('SELECT * FROM events WHERE PersonId=? AND start BETWEEN $begindate AND $endDate');
            $statement->bindParam(1, $persoonId, \PDO::PARAM_INT);
            $statement->bindParam(2, $begindate, \PDO::PARAM_STR);
            $statement->bindParam(3, $endDate, \PDO::PARAM_STR);
            $statement->execute();
            $eventArray = array();
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);

            for ($i = 0; $i < count($result); $i++) {
                $eventArray[$i] = new Event($result[$i]['Id'], $result[$i]['name'], $result[$i]['start'], $result[$i]['end'], $result[$i]['location'], $result[$i]['cost'], $result[$i]['personId']);
            }
            return $eventArray;
        } catch (\Exception $ex) {
            return null;
        }
    }

    public function insertEvent(Event $event)
    {
        var_dump('NOK');
        try {
            $statement = $this->connection->prepare('INSERT INTO events (name, start, end, location, cost, personId) VALUES(?,?,?,?,?,?)');
            $statement->bindParam(1, $event->getEventName(), \PDO::PARAM_STR);
            $statement->bindParam(2, $event->getEventStart(), \PDO::PARAM_STR);
            $statement->bindParam(3, $event->getEventEnd(), \PDO::PARAM_STR);
            $statement->bindParam(4, $event->getEventLocation(), \PDO::PARAM_STR);
            $statement->bindParam(5, $event->getCost(), \PDO::PARAM_STR);
            $statement->bindParam(6, $event->getPersonId(), \PDO::PARAM_STR);

            $statement->execute();
            var_dump('OK');
        } catch (\Exception $ex) {
            return null;
        }
    }
}