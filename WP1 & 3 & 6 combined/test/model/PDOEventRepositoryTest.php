<?php
/**
 * Created by PhpStorm.
 * User: 11502791
 * Date: 14/04/2017
 * Time: 9:46
 */

require_once('src/autoload.php');

use  \model\Event;
use  \model\PDOEventRepository;

class PDORepositoryTest extends PHPUnit\Framework\TestCase
{
    public function setUp()
    {
        $this->mockPDO = $this->getMockBuilder('PDO')
            ->disableOriginalConstructor()
            ->getMock();

        $this->mockPDOStatement = $this->getMockBuilder('PDOStatement')
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function tearDown()
    {
        $this->mockPDO = null;
        $this->mockPDOStatement = null;
    }

    // Begin findEventById() Tests

    public function testFindEventById_idExists_EventObject()
    {
        $event = new Event(1, 'testevent');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([['Id' => $event->getEventId(), 'name' => $event->getEventName(), 'start' => $event->getEventStart(), 'end' => $event->getEventEnd(), 'location' => $event->getEventLocation(), 'cost' => $event->getCost(), 'personId' => $event->getPersonId()]]));

        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $actualEvent = $pdoRepository->findEventById($event->getEventId());

        $this->assertEquals($event, $actualEvent);
    }

    public function testFindEventById_idDoesNotExist_Exception()
    {
        $this->expectException(Exception::class);
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([]));

        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $actualEvent = $pdoRepository->findEventById(6669);

        $this->assertNull($actualEvent);
    }


    public function testFindEventById_exeptionThrownFromPDO_Exeption()
    {
        $this->expectException(Exception::class);

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam')->will($this->throwException(new Exception()));

        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $pdoRepository->findEventById(1);

    }

    // End findEventById() Tests

    // Begin findEvents() Tests*/

    public function testFindEvents_ArrayOfEventObjects()
    {
        $eventArray = array();

        $eventArray[0] = new Event(1, 'test', null, null, null, null, null);
        $eventArray[1] = new Event(2, 'test2', null, null, null, null, null);

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([
                ['Id' => $eventArray[0]->getEventId(), 'name' => $eventArray[0]->getEventName(), 'start' => $eventArray[0]->getEventStart(), 'end' => $eventArray[0]->getEventEnd(), 'location' => $eventArray[0]->getEventLocation(), 'cost' => $eventArray[0]->getCost(), 'personId' => $eventArray[0]->getPersonId()],
                ['Id' => $eventArray[1]->getEventId(), 'name' => $eventArray[1]->getEventName(), 'start' => $eventArray[1]->getEventStart(), 'end' => $eventArray[1]->getEventEnd(), 'location' => $eventArray[1]->getEventLocation(), 'cost' => $eventArray[1]->getCost(), 'personId' => $eventArray[1]->getPersonId()]
            ]));

        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $actualEventArray = $pdoRepository->findEvents();

        $this->assertEquals($eventArray, $actualEventArray);
    }

    public function testFindEvents_DBIsEmpty_EmptyArray()
    {
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([]));

        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $actualEventArray = $pdoRepository->findEvents();

        // fwrite(STDERR, print_r($actualEventArray, TRUE));

        $this->assertEmpty($actualEventArray);
    }

    // End findEvents() Tests

    // Begin findEventByPersonId() Tests

    public function testFindEventByPersonId_idExists_ArrayOfEventObjects()
    {
        $eventArray = array();
        $eventArray[0] = new Event(1, 'test', null, null, null, null, 6);
        $eventArray[1] = new Event(2, 'test2', null, null, null, null, 6);

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([
                ['Id' => $eventArray[0]->getEventId(), 'name' => $eventArray[0]->getEventName(), 'start' => $eventArray[0]->getEventStart(), 'end' => $eventArray[0]->getEventEnd(), 'location' => $eventArray[0]->getEventLocation(), 'cost' => $eventArray[0]->getCost(), 'personId' => $eventArray[0]->getPersonId()],
                ['Id' => $eventArray[1]->getEventId(), 'name' => $eventArray[1]->getEventName(), 'start' => $eventArray[1]->getEventStart(), 'end' => $eventArray[1]->getEventEnd(), 'location' => $eventArray[1]->getEventLocation(), 'cost' => $eventArray[1]->getCost(), 'personId' => $eventArray[1]->getPersonId()]
            ]));

        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $actualEventArray = $pdoRepository->findEventByPerson(6);

        $this->assertEquals($eventArray, $actualEventArray);
    }

    public function testFindEventByPersonId_idDoesNotExistInEventTable_EmptyArray()
    {
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([]));

        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $actualEventArray = $pdoRepository->findEventByPerson(6);

        $this->assertEmpty($actualEventArray);
    }

    // End findEventByPersonId() Tests

    // Begin findEventByDate() Tests

    public function testFindEventByDate_FindsDateMatch_ArrayOfEventObjects()
    {
        $eventArray = array();
        $eventArray[0] = new Event(1, 'testevent1', DateTime::createFromFormat('Y-m-!d H:i', '1995-02-15 15:16'), DateTime::createFromFormat('Y-m-!d H:i', '2009-02-15 15:16'), null, null, null, null);
        $eventArray[1] = new Event(2, 'testevent2', DateTime::createFromFormat('Y-m-!d H:i', '1999-02-15 15:16'), DateTime::createFromFormat('Y-m-!d H:i', '2005-02-15 15:16'), null, null, null, null);

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([
                ['Id' => $eventArray[0]->getEventId(), 'name' => $eventArray[0]->getEventName(), 'start' => $eventArray[0]->getEventStart(), 'end' => $eventArray[0]->getEventEnd(), 'location' => $eventArray[0]->getEventLocation(), 'cost' => $eventArray[0]->getCost(), 'personId' => $eventArray[0]->getPersonId()],
                ['Id' => $eventArray[1]->getEventId(), 'name' => $eventArray[1]->getEventName(), 'start' => $eventArray[1]->getEventStart(), 'end' => $eventArray[1]->getEventEnd(), 'location' => $eventArray[1]->getEventLocation(), 'cost' => $eventArray[1]->getCost(), 'personId' => $eventArray[1]->getPersonId()]
            ]));

        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $actualEventArray = $pdoRepository->findEventByDate(DateTime::createFromFormat('Y-m-!d H:i', '1990-02-15 15:16'), DateTime::createFromFormat('Y-m-!d H:i', '2010-02-15 15:16'));

        $this->assertEquals($eventArray, $actualEventArray);
    }

    public function testFindEventByDate_FindsNoDateMatch_EmptyArray()
    {
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([]));

        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $actualEventArray = $pdoRepository->findEventByDate(DateTime::createFromFormat('Y-m-!d H:i', '1990-02-15 15:16'), DateTime::createFromFormat('Y-m-!d H:i', '2010-02-15 15:16'));

        $this->assertEmpty($actualEventArray);
    }

    // End findEventByDate() Tests

    // Begin findEventByPersonAndDate() Tests

    public function testFindEventByDateAndPerson_FindsDateMatch_ArrayOfEventObjects()
    {
        $eventArray = array();
        $eventArray[0] = new Event(1, 'testevent1', DateTime::createFromFormat('Y-m-!d H:i', '1995-02-15 15:16'), DateTime::createFromFormat('Y-m-!d H:i', '2009-02-15 15:16'), null, null, null, 6);
        $eventArray[1] = new Event(2, 'testevent2', DateTime::createFromFormat('Y-m-!d H:i', '1999-02-15 15:16'), DateTime::createFromFormat('Y-m-!d H:i', '2005-02-15 15:16'), null, null, null, 6);

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([
                ['Id' => $eventArray[0]->getEventId(), 'name' => $eventArray[0]->getEventName(), 'start' => $eventArray[0]->getEventStart(), 'end' => $eventArray[0]->getEventEnd(), 'location' => $eventArray[0]->getEventLocation(), 'cost' => $eventArray[0]->getCost(), 'personId' => $eventArray[0]->getPersonId()],
                ['Id' => $eventArray[1]->getEventId(), 'name' => $eventArray[1]->getEventName(), 'start' => $eventArray[1]->getEventStart(), 'end' => $eventArray[1]->getEventEnd(), 'location' => $eventArray[1]->getEventLocation(), 'cost' => $eventArray[1]->getCost(), 'personId' => $eventArray[1]->getPersonId()]
            ]));

        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $actualEventArray = $pdoRepository->findEventByPersonAndDate(6, DateTime::createFromFormat('Y-m-!d H:i', '1990-02-15 15:16'), DateTime::createFromFormat('Y-m-!d H:i', '2010-02-15 15:16'));

        $this->assertEquals($eventArray, $actualEventArray);
    }

    public function testFindEventByDateAndPerson_FindsNoPersonMatch_EmptyArray()
    {
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([]));

        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $actualEventArray = $pdoRepository->findEventByPersonAndDate(8, DateTime::createFromFormat('Y-m-!d H:i', '1990-02-15 15:16'), DateTime::createFromFormat('Y-m-!d H:i', '2010-02-15 15:16'));

        $this->assertEmpty($actualEventArray);
    }

    public function testFindEventByDateAndPerson_FindsNoDateMatch_EmptyArray()
    {
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([]));

        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $actualEventArray = $pdoRepository->findEventByPersonAndDate(8, DateTime::createFromFormat('Y-m-!d H:i', '1990-02-15 15:16'), DateTime::createFromFormat('Y-m-!d H:i', '2010-02-15 15:16'));

        $this->assertEmpty($actualEventArray);
    }

    // Begin findEventByPersonAndDate() Tests

    // Begin insertEvent() Tests

    public function testInsertEvent_ValidEvent()
    {

        $event = new Event(1, 'testevent1', DateTime::createFromFormat('Y-m-!d H:i', '1995-02-15 15:16'), DateTime::createFromFormat('Y-m-!d H:i', '2009-02-15 15:16'), 'place de la republique', 500, 6);

        $this->mockPDOStatement
            ->expects($this->atLeastOnce())
            ->method('bindParam');

        $this->mockPDOStatement
            ->expects($this->atLeastOnce())
            ->method('execute');

        $this->mockPDO
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $pdoRepository->insertEvent($event);
    }

    // End insertEvent() Tests

    // Begin deleteEvent() Tests

    /*public function testDeleteEvent_idExists() {

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute')
            ->will($this->returnValue([]));

        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $pdoRepository->deleteEvent(5);
    }

    // End deleteEvent() Tests*/

}