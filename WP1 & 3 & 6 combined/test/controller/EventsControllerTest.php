<?php

use \controller\EventsController;
use \model\Event;

require_once ('src/autoload.php');

class EventsControllerTest extends PHPUnit\Framework\TestCase
{
    public function setUp()
    {
        $this->mockEventRepository = $this->getMockBuilder('model\EventRepository')
            ->getMock();
        $this->mockView = $this->getMockBuilder('view\View')
            ->getMock();
    }

    public function tearDown()
    {
        $this->mockEventRepository = null;
        $this->mockView = null;
    }

    public function testHandleFindEventById_eventFound_stringWithIdName()
    {
        $event = new Event(1, 'testevent1', 1, 1, 'testlocation', 1);
        $this->mockEventRepository->expects($this->atLeastOnce())
            ->method('findEventById')
            ->will($this->returnValue($event));

        $this->mockView->expects($this->atLeastOnce())
            ->method('show')
            ->will($this->returnCallback(function ($object) {
                $event = $object['event'];
                printf("%d %s", $event->getEventId(), $event->getEventName());
            }));

        $eventController = new EventsController($this->mockEventRepository, $this->mockView);
        $eventController->handleFindEventById($event->getEventId());
        $this->expectOutputString(printf("%d %s", $event->getEventId(), $event->getEventName()));
    }


    public function test_handleFindEventById_EventFound_returnStringEmpty()
    {
        $this->mockEventRepository->expects($this->atLeastOnce())
            ->method('findEventById')
            ->will($this->returnValue(null));

        $this->mockView->expects($this->atLeastOnce())
            ->method('show')
            ->will($this->returnCallback(function ($object) {
                echo '';
            }));

        $eventController = new EventsController($this->mockEventRepository, $this->mockView);
        $eventController->handleFindEventById(1);
        $this->expectOutputString('');
    }



    public function testHandleFindEventByPerson_eventFound_stringWithIdNamePerson()
    {
        $event = new Event(1, 'testevent1', 4);
        $this->mockEventRepository->expects($this->atLeastOnce())
            ->method('findEventByPerson')
            ->will($this->returnValue($event));

        /*$this->mockView->expects($this->atLeastOnce())
            ->method('show')
            ->will($this->returnCallback(function ($object) {
                $event = $object['event'];
                printf("%d %s", $event->getEventId(), $event->getEventName(), $event->getPersonId());
            }));*/

        $eventController = new EventsController($this->mockEventRepository, $this->mockView);
        $eventController->handleFindEventByPerson($event->getPersonId());
        $this->expectOutputString(printf("%d %s", $event->getEventId(), $event->getEventName(), $event->getPersonId()));
    }

    public function test_handleFindEventByPersonId_EventFound_returnStringEmpty()
    {
        $this->mockEventRepository->expects($this->atLeastOnce())
            ->method('findEventByPerson')
            ->will($this->returnValue(null));

        $this->mockView->expects($this->atLeastOnce())
            ->method('show')
            ->will($this->returnCallback(function ($object) {
                echo '';
            }));

        $eventController = new EventsController($this->mockEventRepository, $this->mockView);
        $eventController->handleFindEventByPerson(1);
        $this->expectOutputString('');
    }

    public function testHandleFindEventByDate_eventFound_stringWithIdNameDate()
    {
        $event = new Event(1, 'testevent1', DateTime::createFromFormat('Y-m-!d H:i', '1995-02-15 15:16'), DateTime::createFromFormat('Y-m-!d H:i', '2009-02-15 15:16'), 'place de la repulique', 500.5, 6);
        $this->mockEventRepository->expects($this->atLeastOnce())
            ->method('findEventByDate')
            ->will($this->returnValue($event));

        /*$this->mockView->expects($this->atLeastOnce())
            ->method('show')
            ->will($this->returnCallback(function ($object) {
                $event = $object['event'];
                printf("%d %s", $event->getEventId(), $event->getEventName(), $event->getEventStart(), $event->getEventEnd());
            }));*/

        $eventController = new EventsController($this->mockEventRepository, $this->mockView);
        $eventController->handleFindEventByDate($event->getEventStart(), $event->getEventEnd());
        $this->expectOutputString(printf("%d %s", $event->getEventId(), $event->getEventName(), $event->getEventStart(), $event->getEventEnd()));
    }

    public function test_handleFindEventByDate_EventFound_returnStringEmpty()
    {
        $this->mockEventRepository->expects($this->atLeastOnce())
            ->method('findEventByDate')
            ->will($this->returnValue(null));

        $this->mockView->expects($this->atLeastOnce())
            ->method('show')
            ->will($this->returnCallback(function ($object) {
                echo '';
            }));

        $eventController = new EventsController($this->mockEventRepository, $this->mockView);
        $eventController->handleFindEventByDate(DateTime::createFromFormat('Y-m-!d H:i', '1990-02-15 15:16'), DateTime::createFromFormat('Y-m-!d H:i', '2010-02-15 15:16'));
        $this->expectOutputString('');
    }

   public function testHandleFindEventByPersonAndDate_eventFound_stringWithIdNamePersonDate()
    {
        $event = new Event(1, 'testevent1', DateTime::createFromFormat('Y-m-!d H:i', '1995-02-15 15:16'), DateTime::createFromFormat('Y-m-!d H:i', '2009-02-15 15:16'), 'place de la repulique', 500.5, 6);
        $this->mockEventRepository->expects($this->atLeastOnce())
            ->method('findEventByPersonAndDate')
            ->will($this->returnValue($event));

        /*$this->mockView->expects($this->atLeastOnce())
            ->method('show')
            ->will($this->returnCallback(function ($object) {
                $event = $object['event'];
                printf("%d %s", $event->getEventId(), $event->getEventName(), $event->getPersonId(), $event->getEventStart(), $event->getEventEnd());
            }));*/

        $eventController = new EventsController($this->mockEventRepository, $this->mockView);
        $eventController->handleFindEventByPersonAndDate($event->getPersonId(), $event->getEventStart(), $event->getEventEnd());
        $this->expectOutputString(printf("%d %s", $event->getEventId(), $event->getEventName(), $event->getPersonId(), $event->getEventStart(), $event->getEventEnd()));
    }

    public function test_handleFindEventByPersonAndDate_EventFound_returnStringEmpty()
    {
        $this->mockEventRepository->expects($this->atLeastOnce())
            ->method('findEventByPersonAndDate')
            ->will($this->returnValue(null));

        $this->mockView->expects($this->atLeastOnce())
            ->method('show')
            ->will($this->returnCallback(function ($object) {
                echo '';
            }));

        $eventController = new EventsController($this->mockEventRepository, $this->mockView);
        $eventController->handleFindEventByPersonAndDate(1,DateTime::createFromFormat('Y-m-!d H:i', '1990-02-15 15:16'), DateTime::createFromFormat('Y-m-!d H:i', '2010-02-15 15:16'));
        $this->expectOutputString('');
    }

    /*public function testHandleInsertEvent()
    {
        $event = new Event(1, 'testevent', DateTime::createFromFormat('Y-m-!d H:i', '1995-02-15 15:16'), DateTime::createFromFormat('Y-m-!d H:i', '2009-02-15 15:16'), 'place de la repulique', 500.5, 6);
        $this->mockEventRepository->expects($this->atLeastOnce())
            ->method('insertEvent')
            ->will($this->returnValue($event));

        $this->mockView->expects($this->atLeastOnce())
            ->method('show')
            ->will($this->returnCallback(function ($object) {
                $event = $object['event'];
                printf("%d %s", $event->getEventId(), $event->getEventName());
            }));

        $eventController = new EventsController($this->mockEventRepository, $this->mockView);
        $eventController->handleCreateEvent(new Event(1, 'testevent', DateTime::createFromFormat('Y-m-!d H:i', '1995-02-15 15:16'), DateTime::createFromFormat('Y-m-!d H:i', '2009-02-15 15:16'), 'place de la repulique', 500.5, 6));
        $this->expectOutputString(printf("%d %s", $event->getEventId(), $event->getEventName()));
    }*/

    /*public function testHandleDeleteEvent()
    {
        $event = new Event(1, 'testevent1', DateTime::createFromFormat('Y-m-!d H:i', '1995-02-15 15:16'), DateTime::createFromFormat('Y-m-!d H:i', '2009-02-15 15:16'), 'place de la repulique', 500.5, 6);
        $this->mockEventRepository->expects($this->atLeastOnce())
            ->method('insertEvent')
            ->will($this->returnValue($event));

        $this->mockView->expects($this->atLeastOnce())
            ->method('show')
            ->will($this->returnCallback(function ($object) {
                $event = $object['event'];
                printf("%d %s", $event->getEventId(), $event->getEventName(), $event->getPersonId(), $event->getEventStart(), $event->getEventEnd());
            }));

        $eventController = new EventsController($this->mockEventRepository, $this->mockView);
        $eventController->handleFindEventByPersonAndDate($event->getPersonId(), $event->getEventStart(), $event->getEventEnd());
        $this->expectOutputString(sprintf("%d %s", $event->getEventId(), $event->getEventName(), $event->getPersonId(), $event->getEventStart(), $event->getEventEnd()));
    }*/
}
