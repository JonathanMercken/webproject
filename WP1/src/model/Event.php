<?php
namespace model;
/**
 * Created by PhpStorm.
 * User: Stef
 * Date: 22/03/2017
 * Time: 16:38
 */
class Event
{
    private $eventId = null;
    private $eventName = "";
    private $eventStart = null;
    private $eventEnd = null;
    private $eventLocation = null; // new Adres();
    private $eventCost = null;
    private $personId = null;

    public function __construct($id = null, $name = "", $start = null, $end = null, $location = null, $cost = null, $personId = null) {
        $this->eventId = $id;
        $this->eventName = $name;
        $this->eventStart = $start;
        $this->eventEnd = $end;
        $this->eventLocation = $location;
        $this->eventCost = $cost;
        $this->personId = $personId;
        }

    /**
     * @return null
     */
    public function getEventId()
    {
        return $this->eventId;
    }
    /**
     * @param null $eventId
     */
    public function setEventId($eventId)
    {
        $this->eventId = $eventId;
    }
    /**
     * @return string
     */
    public function getEventName()
    {
        return $this->eventName;
    }
    /**
     * @param string $eventName
     */
    public function setEventName($eventName)
    {
        $this->eventName = $eventName;
    }
    /**
     * @return null
     */
    public function getEventStart()
    {
        return $this->eventStart;
    }
    /**
     * @param null $eventStart
     */
    public function setEventStart($eventStart)
    {
        $this->eventStart = $eventStart;
    }
    /**
     * @return null
     */
    public function getEventEnd()
    {
        return $this->eventEnd;
    }
    /**
     * @param null $eventEnd
     */
    public function setEventEnd($eventEnd)
    {
        $this->eventEnd = $eventEnd;
    }
    /**
     * @return Adres|null
     */
    public function getEventLocation()
    {
        return $this->eventLocation;
    }
    /**
     * @param Adres|null $eventLocation
     */
    public function setEventLocation($eventLocation)
    {
        $this->eventLocation = $eventLocation;
    }
    /**
     * @return null
     */
    public function getPersonId()
    {
        return $this->personId;
    }
    /**
     * @param null $personId
     */
    public function setPersonId($personId)
    {
        $this->personId = $personId;
    }

    public function getCost() {
        return $this->eventCost;
    }
    public function setCost($cost) {
        if($cost > 0 && is_numeric($cost)) {
            $this->eventCost = $cost;
        }
    }




}