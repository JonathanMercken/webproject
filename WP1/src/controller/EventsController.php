<?php
namespace controller;
use model\EventRepository;
use view\View;
use \model\Event;
/**
 * Created by PhpStorm.
 * User: Stef
 * Date: 22/03/2017
 * Time: 16:33
 */
class EventsController
{
    private $eventRepository;
    private $view;

    public function __construct(EventRepository $eventRepository, View $view)
    {
        $this->eventRepository = $eventRepository;
        $this->view = $view;
    }

    public function handleFindEventById($id = null)
    {
        $event = $this->eventRepository->findEventById($id);
        $this->view->show(['event'=>$event]);
    }

    public function handleFindEvents()
    {
        $events = $this->eventRepository->FindEvents();
        $this->view->show(['events'=>$events]);
    }

    public function handleFindEventByPerson($personId)
    {
        $event = $this->eventRepository->findEventByPerson($personId);
        $this->view->show(['events'=>$event]);
    }

    public function handleFindEventByDate($begindate, $endDate)
    {
        $event = $this->eventRepository->findEventByDate($begindate, $endDate);
        $this->view->show(['events'=>$event]);
    }

    public function handleFindEventByPersonAndDate($personId, $begindate, $endDate)
    {
        $event = $this->eventRepository->findEventByPersonAndDate($personId, $begindate, $endDate);
        $this->view->show(['events'=>$event]);
    }

    public function handleCreateEvent(Event $event)
    {
        $this->eventRepository->insertEvent($event);
    }

    public function handleDeleteEvent($id)
    {
        $this->eventRepository->deleteEvent($id);
        $this->view->show(['deleteMessage'=>$id]);
    }
}