-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Gegenereerd op: 23 mei 2017 om 14:14
-- Serverversie: 5.7.18-0ubuntu0.16.04.1
-- PHP-versie: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webproject`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `events`
--

CREATE TABLE `events` (
  `Id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `location` varchar(200) NOT NULL,
  `cost` decimal(10,2) NOT NULL,
  `personId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `events`
--

INSERT INTO `events` (`Id`, `name`, `start`, `end`, `location`, `cost`, `personId`) VALUES
(3, 'Monkey Business Live show', '2017-06-22 17:00:00', '2017-07-22 19:00:00', 'Anspachlaan 110,\r\n1000 Brussel', '1500.00', 0),
(4, 'Circus Elias', '2017-05-10 13:30:00', '2017-05-10 17:30:00', 'Korda Kampus', '0.00', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `login`
--

INSERT INTO `login` (`id`, `username`, `password`) VALUES
(1, 'webproject', 'pxlweb456');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `persons`
--

CREATE TABLE `persons` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `location` varchar(200) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `cell` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `persons`
--

INSERT INTO `persons` (`id`, `name`, `first_name`, `location`, `phone`, `cell`, `email`, `type`) VALUES
(1, 'Vanelchteren', 'Erik', 'Meerdegatstraat 21a, 3570 Alken', '', '+32498542391', 'erik.vanelchteren@telenet.be', 'Advocaten'),
(2, 'Sleutels', 'Alicia', 'Vuurkruisenlaan 35, 3500 Hasselt', '+3211745898', '+32488551277', 'alicia.sleutels@hotmail.com', 'Janetten'),
(3, 'Klapperkaak', 'Jaak', 'straatlaanweg 11, 1000 Brussel', '', '+32494444444', 'jaak.klapperkaak@gmail.com', 'standup'),
(4, 'Gelders', 'Frank', 'Luiksersteenweg 48, 3500 Hasselt', '+3211765498', '', 'gelders.frank@telenet.be', 'Chef-koks');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Tickets`
--

CREATE TABLE `Tickets` (
  `Id` int(11) NOT NULL,
  `Naam` varchar(30) NOT NULL,
  `Voornaam` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `Tickets`
--

INSERT INTO `Tickets` (`Id`, `Naam`, `Voornaam`) VALUES
(1, 'Hart', 'Jacques'),
(2, 'Roex', 'Elias'),
(3, 'Roex', 'Elias');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`Id`);

--
-- Indexen voor tabel `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `persons`
--
ALTER TABLE `persons`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `Tickets`
--
ALTER TABLE `Tickets`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `events`
--
ALTER TABLE `events`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT voor een tabel `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT voor een tabel `persons`
--
ALTER TABLE `persons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT voor een tabel `Tickets`
--
ALTER TABLE `Tickets`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
