<?php
/**
 * Created by PhpStorm.
 * User: jonathan
 * Date: 29/03/2017
 * Time: 14:29
 */

namespace view;

class EventJsonView implements View
{
    public function show(array $data)
    {
        header('Content-Type: application/json');
        if (isset($data['event'])) {
            $event = $data['event'];
            echo json_encode(['id' => $event->getEventId(),
                'name' => $event->getEventName(),
                'start' => $event->getEventStart(),
                'end' => $event->getEventEnd(),
                'location' => $event->getEventLocation(),
                'cost' => $event->getCost()]);

        } elseif (isset($data['events'])){
            $events = $data['events'];

            $eventArr = [];
            foreach ($events as $event) {
                $s = json_encode(['id' => $event->getEventId(),
                    'name' => $event->getEventName(),
                    'start' => $event->getEventStart(),
                    'end' => $event->getEventEnd(),
                    'location' => $event->getEventLocation(),
                    'cost' => $event->getCost()]);
                $eventArr[] = $s;
            }
            echo "[" . implode(",", $eventArr) . "]";
        }else {
            echo '{}';
        }

    }

}