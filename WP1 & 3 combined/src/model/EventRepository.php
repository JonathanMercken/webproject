<?php
/**
 * Created by PhpStorm.
 * User: jonathan
 * Date: 29/03/2017
 * Time: 14:28
 */

namespace model;


interface EventRepository
{
 function findEventById($id);
 function findEvents();
 function findEventByPerson($personId);
 function findEventByDate($beginDate, $endDate);
 function findEventByPersonAndDate($personId, $begindate, $endDate);
 function insertEvent(Event $event);
 function deleteEvent($id);
}